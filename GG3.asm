DATA SEGMENT
    KAL1 DB 10,13,'Masukkan angka yang ingin dicek : $'
    KAL2 DB 10,13,'Nilai yang diinputkan adalah genap$'
    KAL3 DB 10,13,'Nilai yang diinputkan adalah ganjil$'
DATA ENDS

TAMPIL MACRO KAL
    MOV AH,9    
    LEA DX,KAL  
    INT 21H     
ENDM

CODE SEGMENT
    ASSUME CS:CODE,DS:DATA
MULAI:
    MOV AX,DATA
    MOV DS,AX

    TAMPIL KAL1

    MOV AH,1    
    INT 21H     
    MOV AH,0    
CHECK:  
    MOV DL,2    
    DIV DL      
    CMP AH,0    
    JNE GANJIL  
GENAP:
    TAMPIL KAL2
    JMP SELESAI 
GANJIL:
    TAMPIL KAL3

SELESAI:
    MOV AH,4CH  
    INT 21H
CODE ENDS

END MULAI